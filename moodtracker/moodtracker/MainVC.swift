//
//  ViewController.swift
//  moodtracker
//
//  Created by Bruce Tyson on 2/17/17.
//  Copyright © 2017 Bruce Tyson. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var moodPicker: UIPickerView!
    @IBOutlet weak var moodPickerBtn: UIButton!
    
    
    
    
    let moods = ["Happy","Blah", "Sad","Tired","Depressed","Stuck"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        moodPicker.dataSource = self
        moodPicker.delegate = self
        
        self.view.backgroundColor = UIColor.white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func moodBtnPressed(_ sender: AnyObject) {
        
                moodPicker.isHidden = false
        
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return moods.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return moods[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        moodPickerBtn.setTitle(moods[row], for: UIControlState()) //UIControlState.normal
        moodPicker.isHidden = true
    }


}

